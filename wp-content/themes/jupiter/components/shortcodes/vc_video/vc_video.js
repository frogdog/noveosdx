(function($) {
	'use strict';

	function mk_play_videos() {

		// Get lightbox width
		function getLightboxWidth() {
			// Get 90% of screen height
			var $height = Math.ceil(($(window).height() * 90) / 100);

			// Set the screen base lightbox width based on calculted screen height above
			var $width = Math.ceil(($height * 100) / 56.25) - 18 + 'px';
			
			// Calulate the screen width if the height viewport is higher the width
			if($(window).height() >= $(window).width()){
				$width = ($(window).width() - 90) + 'px';
			}
			return $width;
		}

		// Get lightbox height
		function getLightboxHeight() {
			// Set the lightbox height 90% of viewport
			var $height = '90%';

			// Set the lightbox height if the height viewport is higher the width
			if($(window).height() >= $(window).width()){
				$height = Math.ceil(((($(window).width() - 90) * 56.25) / 100)) + 'px';
			}
			return $height;
		}

		// Play self hosted video
		function playSelfHosted(video, isLightbox) {
			if (isLightbox === undefined || typeof isLightbox === 'undefined') {
				isLightbox = false;
			}

			if (isLightbox) {

				var content = video.parent().html();

				var fancyboxArgs = {
					content: '<div class="fancybox-video">' + $(content).attr('autoplay', 'autoplay').wrap('<div></div>').parent().html() + '</div>',
					padding: 0,
					margin: 0,
					showCloseButton: true,
					autoSize: false,
					width: getLightboxWidth(),
					height: getLightboxHeight(),
					tpl: {
						closeBtn: '<a title="Close" class="fancybox-item fancybox-close fancybox-video-close" href="javascript:;"></a>',
					}
				};

				$.fancybox.open(fancyboxArgs);

				$(window).on('resize orientationChange', function(event) {
					$.fancybox.update({width:getLightboxWidth(),height:getLightboxHeight()});
					$.fancybox.reposition();
				});

			} else {
				video.get(0).play();
				video.closest('.video-container').find('.video-thumbnail').fadeOut('slow');
			}
		}

		// Play social hosted video
		function playSocialHosted(iframe, isLightbox) {

			if (isLightbox === undefined || typeof isLightbox === 'undefined') {
				isLightbox = false;
			}

			var src = iframe.attr('src');

			if (isLightbox) {
				var fancyboxArgs = {
					type: 'iframe',
					href: src,
					padding: 0,
					margin: 0,
					showCloseButton: true,
					autoSize: false,
					width: getLightboxWidth(),
					height: getLightboxHeight(),
					tpl: {
						closeBtn: '<a title="Close" class="fancybox-item fancybox-close fancybox-video-close" href="javascript:;"></a>',
					},
					helpers : {
						media: {
							youtube : {
								params : {
									autoplay : 1
								}
							},
							vimeo : {
								params : {
									autoplay : 1
								}
							}
						}
					}
				};

				$.fancybox.open(fancyboxArgs);

				$(window).on('resize orientationChange', function(event) {
					$.fancybox.update({width:getLightboxWidth(),height:getLightboxHeight()});
					$.fancybox.reposition();
				});

			} else {
				var separator = (src.indexOf('?') === -1) ? '?' : '&';
				iframe.get(0).src += separator + 'autoplay=1';
				iframe.closest('.video-container').find('.video-thumbnail').fadeOut('slow');
			}
		}

		$('.video-container').each(function() {

			var $videoContainer = $(this);
			var playSource = $videoContainer.data('source');
			var playTarget = $videoContainer.data('target');
			var $iframe = $videoContainer.find('iframe');
			var $video = $videoContainer.find('video');

			if ($videoContainer.data('autoplay')) {
				switch (playSource) {
					case 'social_hosted':
						playSocialHosted($iframe);
						break;
					case 'self_hosted':
						playSelfHosted($video);
						break;
				}
			} else {
				var $playIcon = $videoContainer.find('.mk-svg-icon');
				$playIcon.bind('click', function(e) {
					e.preventDefault();
					var isLightbox = (playTarget == 'lightbox') ? true : false;
					switch (playSource) {
						case 'social_hosted':
							playSocialHosted($iframe, isLightbox);
							break;
						case 'self_hosted':
							playSelfHosted($video, isLightbox);
							break;
					}
				});
			}
		});
	}
	mk_play_videos();
}(jQuery));