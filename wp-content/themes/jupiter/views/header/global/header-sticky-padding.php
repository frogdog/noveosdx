<?php 
if($view_params['is_shortcode']) return false;

$sticky_style = mk_get_option( 'header_sticky_style', 'fixed' );

if ($sticky_style == 'false' || is_header_transparent()) return false;

?>
<div class="mk-header-padding-wrapper"></div>
