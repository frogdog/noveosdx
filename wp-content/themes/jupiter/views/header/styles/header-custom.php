<?php
/**
 * Template part for custom headers built by Header Builder.
 *
 * @since 5.9 Introduced.
 */
global $mk_options;

$is_shortcode = isset($view_params['is_shortcode']) ? $view_params['is_shortcode'] : false; ?>

<header class="hb-custom-header" <?php echo get_schema_markup('header'); ?>>
	<?php if ( is_header_toolbar_show() === 'true' && $is_shortcode === false ) {
		mk_get_header_view( 'holders', 'toolbar' );
	} ?>
	<div class="hb-devices">
		<?php $mk_hb = new HB_Grid(); ?>
		<?php $mk_hb->render_markup(); ?>
	</div>
	<?php mk_get_view('layout', 'title', false, ['is_shortcode' => $is_shortcode]) ?>
</header>
