<!DOCTYPE html>
<html <?php echo language_attributes();?> >



<head>


<!--GOOGLE ANALYTICS TRACKING CODE-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-103460132-2', 'auto');
  ga('send', 'pageview');

</script>
<!--END GOOGLE ANALYTICS TRACKING CODE-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P62WS2Q');</script>
<!-- End Google Tag Manager -->



<!--GOOGLE VERIFICATION CODE-->
<meta name="google-site-verification" content="Su5zO4KvxUmdfJ5eOHE5iu34AiWRRW6CbZ8N2nDXRm0" />
<!--END GOOFLE VERIFICATION CODE-->

<!--BING TRACKING CODE-->
<meta name="msvalidate.01" content="1ECD24B7C486F3A8F937FB9014F139FC" />
<meta name="description" content="">
<meta name="keywords" content="">
<!--END BING TRACKING CODE-->


<title>VIVO Agency–Device Diagnostics and Health IT Marketing Agency</title>


<!--
/**
 * @license
 * MyFonts Webfont Build ID 3425427, 2017-07-20T10:03:03-0400
 * 
 * The fonts listed in this notice are subject to the End User License
 * Agreement(s) entered into by the website owner. All other parties are 
 * explicitly restricted from using the Licensed Webfonts(s).
 * 
 * You may obtain a valid license at the URLs below.
 * 
 * Webfont: Axiforma-Heavy by Kastelov
 * URL: https://www.myfonts.com/fonts/kastelov/axiforma/heavy/
 * Copyright: Copyright &#x00A9; 2017 by Kastelov. All rights reserved.
 * Licensed pageviews: 10,000
 * 
 * 
 * License: https://www.myfonts.com/viewlicense?type=web&buildid=3425427
 * 
 * © 2017 MyFonts Inc
*/

-->
<link rel="stylesheet" type="text/css" href="../wp-content/fonts/MyFontsWebfontsKit-4/MyFontsWebfontsKit.css">
<?php wp_head(); ?>
<link rel='stylesheet'   href='<?php echo site_url(); ?>/wp-content/themes/jupiter/css/mcsroll.css' type='text/css' />

</head>

<body <?php body_class(mk_get_body_class(global_get_post_id())); ?> <?php echo get_schema_markup('body'); ?> data-adminbar="<?php echo is_admin_bar_showing() ?>" <?php  if(is_page(1824)) { ?> id="About-team"<?php } ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P62WS2Q"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->



	<?php
		// Hook when you need to add content right after body opening tag. to be used in child themes or customisations.
		do_action('theme_after_body_tag_start');
	?>

	<!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->
	<div id="top-of-page"></div>

		<div id="mk-boxed-layout">

			<div id="mk-theme-container" <?php echo is_header_transparent('class="trans-header"'); ?>>

				<?php mk_get_header_view('styles', 'header-'.get_header_style());