<?php
global $mk_options;

if ($view_params['header_style'] == 3) return false;

if(isset($mk_options['hide_header_nav']) && $mk_options['hide_header_nav'] == 'false') return false;

?>

<div class="mk-nav-responsive-link">
    <div class="mk-css-icon-menu">
        <span style="color:#ffffff;font-weight:700;padding: 10px; display: block;">MENU</span>
    </div>
</div>
