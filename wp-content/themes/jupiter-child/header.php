<?php //@session_start();

 error_reporting(0);

global $queried_object;

$queried_object = get_queried_object();

 //var_dump( $queried_object );

 if($queried_object->taxonomy == 'wpdmcategory' ){

     $cat = $queried_object->term_id;

     $link=get_page_link(6687).'?cat='.$cat;

     header('Location:'.$link);

 }

if(isset($_GET['popup'])) { $_SESSION['popup']=$_GET['popup']; }
//don't redirect if user is admin, if its an archive page, or a post
if(
  !current_user_can('administrator')
  &&
  !is_archive()
  &&
  !is_single()
  &&
  (get_the_ID() != 8059) //my account
  &&
  (get_the_ID() != 8063) //register
  &&
  (get_the_id() != 8076) //login
) {
  if($_SESSION['popup'] == 'us') {
    if(strstr($_SERVER['REQUEST_URI'], '/us/') == false) {
      header('Location: /us' . $_SERVER['REQUEST_URI']);
    }
  } else {
    $uri = $_SERVER['REQUEST_URI'];
    if(substr($uri, 0, 3) == '/us') {
      $uri = substr($uri, 3);
      header('Location: ' . $uri);
    }
  }
}
?>

<!DOCTYPE html>

<html <?php echo language_attributes();?> >

<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-39732767-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-39732767-2');
</script>

<title>HYCOR Biomedical</title>

<!--

/**

 * @license

 * MyFonts Webfont Build ID 3425427, 2017-07-20T10:03:03-0400

 *

 * The fonts listed in this notice are subject to the End User License

 * Agreement(s) entered into by the website owner. All other parties are

 * explicitly restricted from using the Licensed Webfonts(s).

 *

 * You may obtain a valid license at the URLs below.

 *

 * Webfont: Axiforma-Heavy by Kastelov

 * URL: https://www.myfonts.com/fonts/kastelov/axiforma/heavy/

 * Copyright: Copyright &#x00A9; 2017 by Kastelov. All rights reserved.

 * Licensed pageviews: 10,000

 *

 *

 * License: https://www.myfonts.com/viewlicense?type=web&buildid=3425427

 *

 * Â© 2017 MyFonts Inc

*/



-->

<link rel="stylesheet" type="text/css" href="/wp-content/fonts/MyFontsWebfontsKit-4/MyFontsWebfontsKit.css">

<?php wp_head(); ?>

<link rel='stylesheet'   href='<?php echo site_url(); ?>/wp-content/themes/jupiter/css/mcsroll.css' type='text/css' />

</head>


<body <?php body_class(mk_get_body_class(global_get_post_id())); ?> <?php echo get_schema_markup('body'); ?> data-adminbar="<?php echo is_admin_bar_showing() ?>" <?php  if(is_page(1824)) { ?> id="About-team"<?php } ?> data-lang="<?= $_SESSION['popup']; ?>">
<?php

		// Hook when you need to add content right after body opening tag. to be used in child themes or customisations.

		do_action('theme_after_body_tag_start');

	?>

<?php if( (!isset($_SESSION['popup'])) ){ ?>

 <div id="popup1" class="overlay open">

	<div class="popup-outer">

		<div class="popup">

			<h2>Please select  your region</h2>

			<div class="tab">

			  	<button class="tablinks close" data-lang="us" onclick="openCity(event, 'London')">United States</button>

			  	<button class="tablinks close" data-lang="eu" onclick="openCity(event, 'Paris')">Europe</button>

			  	<button class="tablinks" onclick="openCity(event, 'Tokyo')">Other</button>

			</div>

			<div id="London" class="tabcontent">

			  	<p>Thank you for your interest in the NOVEOS™ system. This product is not currently available in the United States and in some regions outside of Europe.</p>

			  	<a href="http://www.hycorbiomedical.com/sites/usa/" target="_blank">Visit HYCOR</a>

			</div>

			<div id="Tokyo" class="tabcontent">

<p>The NOVEOS™ system is CE marked and FDA cleared. This product is not currently available in some regions outside the United States and Europe.</p>
			  	<a href="http://www.hycorbiomedical.com/sites/int/" target="_blank">Visit HYCOR</a>

			</div>

		</div>

	</div>

</div>

<?php }?>

	<!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->

	<div id="top-of-page"></div>



		<div id="mk-boxed-layout">



			<div id="mk-theme-container" <?php echo is_header_transparent('class="trans-header"'); ?>>



 <?php mk_get_header_view('styles', 'header-'.get_header_style());
