<?php
/*
 * Template Name: Resource Category Template New
** resource-category-template.php
** mk_build_main_wrapper : builds the main divisions that contains the content. Located in framework/helpers/global.php
** mk_get_view gets the parts of the pages, modules and components. Function located in framework/helpers/global.php
*/
get_header();

Mk_Static_Files::addAssets('mk_button');
Mk_Static_Files::addAssets('mk_audio');
Mk_Static_Files::addAssets('mk_swipe_slideshow');
mk_build_main_wrapper( mk_get_view('singular', 'wp-page', true) );
if(isset($_GET['cat'])){
$Category = get_term_by( 'id', $_GET['cat'], 'wpdmcategory');
//print_r($Category);
$output_string="";
$parent_terms = get_terms( 'wpdmcategory', array( 'parent' =>$_GET['cat'], 'orderby' => 'slug', 'hide_empty' => false ) );
$childsize=sizeof($parent_terms);
}
if(!isset($_GET['q'])){
 if($childsize > 0){
 ?>
<div class="wpb_row vc_row vc_row-fluid  mk-fullwidth-true  attched-false js-master-row  mk-in-viewport">
<div style="" class="vc_col-sm-12 container-main wpb_column column_container  _ height-full">
		<div id="animated-columns-7" class="mk-animated-columns clearfix full-style a_5col o3col o2col has-border  mk-animate-element scale-up   mk-in-viewport">
		<?php $loop=0; ?>
                <?php foreach ( $parent_terms as $pterm ) { $loop++;
 $term_id = $pterm->term_id;?>
<!--            print_r($term);-->
                    <div class="animated-column-item s_item a_colitem a_position-relative a_opacity-0 a_float-left a_overflow-hidden a_align-center a_box-border subcategory-bg<?php echo $loop; ?>" aria-haspopup="true" style="height: 582px; opacity: 1; background-color: #e5f6fd; border: 1px solid #fff;">
			<a href="<?php echo get_category_link($pterm->term_id); ?>" >
                        <div class="animated-column-holder a_position-absolute a_width-100-per a_display-block padding-20 a_top-0 a_box-border" style="padding-top: 227px; top: -0.968618%;">
                            <div class="a_padding-bottom-30 categories-icon">
			        <img src="<?php echo get_field('category_icon','wpdmcategory_'.$term_id); ?>" class="mk-svg-icon" style="height: 64px; width: 64px;" viewBox="0 0 512 512" />
                            </div>
                            <div class="animated-column-title s_title a_position-relative a_font-weight-bold a_text-transform-up a_box-border" style="height: 46px;">
                               <?php echo $pterm->name;?>
                            </div>
			</div>

                        <p class="animated-column-desc s_desc a_top-100-per a_font-14 a_position-relative a_width-100-per a_line-25 a_box-border" style="height: 170px; top: 95.376%;"><?php echo $pterm->description; ?></p>
                        <div class="animated-column-btn a_position-relative a_top-100-per a_width-100-per" style="top: 100%;">
                            <div id="mk-button-8" class="mk-button-container _ relative    block text-center ">
                                    <span class="mk-button--text">Learn More</span>
                            </div>
                        </div>
                        </a>
                    </div>
<style>
    .subcategory-bg<?php echo $loop; ?>:hover
        {
            background-color: <?php echo get_field('mouse_hover_cover','wpdmcategory_'.$term_id); ?> !important;
        }
</style>
		<?php  }
?>
	</div>
	<div class="clearboth"></div>
</div>
	</div>
 <?php } //mk_build_main_wrapper( mk_get_view('singular', 'wp-page', true) );
else { ?>

 <?php } ?>
<?php } ?>

   <style>
   /* .well.c2a3 .btn.wpdm-download-link{ padding: 11px 30px;font-size: 11pt; } */
/*      .well.c2a3 .media-body{ font-size: 11pt; }*/
      /*.well.c2a3 .wpdm_icon{ height: 42px; width: auto; }*/

   /* blockquote:before {
    position: absolute;
    background-image : url(data:image/svg+xml; utf8, <svg class="mk-svg-icon" xmlns="http://www.w3.org�-181 75t-75 181v32q0 40 28 68t68 28h224q80 0 136 56t56 136z"></path></svg>);
    content: "";
    display: none;
    left: 20px;
    top: 20px;
    width: 32px;
    height: 35px;

}*/
  </style>
 <?php

get_footer();
