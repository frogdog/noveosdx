<?php
  // Add Shortcode
  function custom_shortcode() { ?>
        <section id="content" class="teamMember">
        <div class="container">

      <ul id="og-grid" class="og-grid cf">
           <?php   $args=array();
          $args['post_type']= 'employees';
          $args['posts_per_page']= -1;
          $thePosts=query_posts($args);
              while (have_posts()) : the_post();  ?>
              <li>
                  <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');  ?>
                      <a href="#" data-largesrc="<?php echo $featured_img_url; ?>" style="">
                          <img src="<?php echo $featured_img_url; ?>" width="250px" height="250px">
                          <span class="name"><?php echo get_the_title(); ?></span>
                          <span class="sotDis"><?php echo get_post_meta(get_the_ID() , '_position', true); ?></span>

                       </a>

                      <div class="description displayNone og-details">
                          <div class="client-detail">
                          <h3><?php echo get_the_title(); ?></h3>
                          <div class="designation"><?php echo get_post_meta(get_the_ID() , '_position', true); ?></div>
                          <?php   $facebook = get_post_meta(get_the_ID() , '_facebook', true);
                                  $linkedin = get_post_meta(get_the_ID() , '_linkedin', true);
                                  $twitter = get_post_meta(get_the_ID() , '_twitter', true);
                                  $email = get_post_meta(get_the_ID() , '_email', true);
                                  $googleplus = get_post_meta(get_the_ID() , '_googleplus', true);
                                  $instagram = get_post_meta(get_the_ID() , '_instagram', true);  ?>
                          <ul class="mk-employeee-networks social-icon">
                                  <?php if (!empty($email)) { ?>
                                          <li><a target="_blank" href="mailto:<?php echo antispambot($email); ?>" title="<?php _e('Get In Touch With', 'mk_framework'); ?> <?php the_title_attribute(); ?>"><?php Mk_SVG_Icons::get_svg_icon_by_class_name(true, 'mk-icon-envelope', 16); ?></a></li>
                                  <?php }
                                  if (!empty($facebook)) {  ?>
                                          <li><a target="_blank" href="<?php echo $facebook; ?>" title="<?php the_title_attribute(); ?> <?php _e('On', 'mk_framework'); ?> Facebook"><?php Mk_SVG_Icons::get_svg_icon_by_class_name(true, 'mk-moon-facebook', 16); ?></a></li>
                                  <?php }
                                  if (!empty($twitter)) {  ?>
                                          <li><a target="_blank" href="<?php echo $twitter; ?>" title="<?php the_title_attribute(); ?> <?php _e('On', 'mk_framework'); ?> Twitter"><?php Mk_SVG_Icons::get_svg_icon_by_class_name(true, 'mk-moon-twitter', 16); ?></a></li>
                                  <?php }
                                  if (!empty($googleplus)) {  ?>
                                          <li><a target="_blank" href="<?php echo $googleplus; ?>" title="<?php the_title_attribute(); ?> <?php _e('On', 'mk_framework'); ?> Google Plus"><?php Mk_SVG_Icons::get_svg_icon_by_class_name(true, 'mk-moon-google-plus', 16); ?></a></li>
                                  <?php }
                                  if (!empty($linkedin)) {  ?>
                                          <li><a target="_blank" href="<?php echo $linkedin; ?>" title="<?php the_title_attribute(); ?> <?php _e('On', 'mk_framework'); ?> Linked In"><?php Mk_SVG_Icons::get_svg_icon_by_class_name(true, 'mk-jupiter-icon-simple-linkedin', 16); ?></a></li>
                                  <?php }
                                  if (!empty($instagram)) {  ?>
                                          <li><a target="_blank" href="<?php echo $instagram; ?>" title="<?php the_title_attribute(); ?> <?php _e('On', 'mk_framework'); ?> Instagram"><?php Mk_SVG_Icons::get_svg_icon_by_class_name(true, 'mk-icon-instagram', 16); ?></a></li>
                                  <?php }  ?>
                          </ul>
                          </div>
                          <div class='team-detail'>
                              <?php the_content(); ?>
                          </div>
                      </div>
              </li>
              <?php endwhile;  ?>
          <?php wp_reset_query(); ?>
              </ul>
          </div>
      </section>
  <?php }
  add_shortcode( 'team', 'custom_shortcode' );
  // Add Shortcode
  function get_download_category() { ?>
          <div style="" class="vc_col-sm-12 wpb_column column_container  _ height-full">
  		<div id="animated-columns-7" class="mk-animated-columns clearfix full-style a_5col o3col o2col has-border  mk-animate-element scale-up   mk-in-viewport">
  		<?php $myterms = get_terms( array( 'taxonomy' => 'wpdmcategory', 'parent' => 0, 'hide_empty' => false ) );
                   $loop=0;
        foreach( $myterms as $term ) { //echo "<pre>"; print_r($term);
            $loop++;
        $term_id = $term->term_id;?>
  <!--            print_r($term);-->
                      <div class="animated-column-item s_item a_colitem a_position-relative a_opacity-0 a_float-left a_overflow-hidden a_align-center a_box-border subcategory-bg<?php echo $loop; ?>" aria-haspopup="true" style="height: 582px; opacity: 1; background-color: #00a8d4;  box-shadow: 0px 0px 0px 1px #fff;   ">
  			<?php if(get_field('add_page_link','wpdmcategory_'.$term_id)) {?>
                                  <a href="<?php echo get_field('page_link','wpdmcategory_'.$term_id); ?>" >
                                  <?php } else { ?>
                                  <a href="<?php echo get_category_link($term->term_id); ?>" >
                          <?php } ?>
                                  <div class="animated-column-holder a_position-absolute a_width-100-per a_display-block padding-20 a_top-0 a_box-border" style="padding-top: 227px; top: -0.968618%;">
                                      <div class="a_padding-bottom-30 categories-icon">
                                          <img src="<?php echo get_field('category_icon','wpdmcategory_'.$term_id); ?>" class="mk-svg-icon" style="height: 64px; width: 64px;" viewBox="0 0 512 512" />
                                      </div>
                                      <div class="animated-column-title s_title a_position-relative a_font-weight-bold a_text-transform-up a_box-border" style="height: 46px;">
                                          <?php echo $term->name;?>
                                      </div>
                                  </div>
                                  <p class="animated-column-desc s_desc a_top-100-per a_font-14 a_position-relative a_width-100-per a_line-25 a_box-border" style="height: 170px; top: 95.376%;"><?php echo $term->description; ?></p>
                                  <div class="animated-column-btn a_position-relative a_top-100-per a_width-100-per" style="top: 100%;">
                                      <div id="mk-button-8" class="mk-button-container _ relative    block text-center ">
                                              <span class="mk-button--text">Learn More</span>
                                      </div>
                                  </div>
                                  </a>
                      </div>
  <style>
      .subcategory-bg<?php echo $loop; ?>:hover
          {
              background-color: <?php echo get_field('mouse_hover_cover','wpdmcategory_'.$term_id); ?> !important;
          }
  </style>
  		<?php  }
  ?>
  	</div>

  </div>
  <?php }
  add_shortcode( 'download_category', 'get_download_category' );
  function get_resource_category(){

  $parent_terms = get_terms( 'wpdmcategory', array( 'parent' =>$_GET['cat'], 'orderby' => 'slug', 'hide_empty' => false ) );
  $childsize=sizeof($parent_terms);
   if($childsize > 0){

   ?>
  <div class="wpb_row vc_row vc_row-fluid  mk-fullwidth-true  attched-false js-master-row  mk-in-viewport">



  		<div id="animated-columns-7" class="mk-animated-columns clearfix full-style a_5col o3col o2col has-border  mk-animate-element scale-up   mk-in-viewport">
  		<?php $loop=0; ?>
                  <?php foreach ( $parent_terms as $pterm ) { $loop++;
   $term_id = $pterm->term_id;?>
  <!--            print_r($term);-->
                      <div class="animated-column-item s_item a_colitem a_position-relative a_opacity-0 a_float-left a_overflow-hidden a_align-center a_box-border subcategory-bg<?php echo $loop; ?>" aria-haspopup="true" style="height: 582px; opacity: 1; background-color: #e5f6fd; border: 1px solid #fff;">
  			<a href="<?php echo get_category_link($pterm->term_id); ?>" >
                          <div class="animated-column-holder a_position-absolute a_width-100-per a_display-block padding-20 a_top-0 a_box-border" style="padding-top: 227px; top: -0.968618%;">
                              <div class="a_padding-bottom-30 categories-icon">
  			        <img src="<?php echo get_field('category_icon','wpdmcategory_'.$term_id); ?>" class="mk-svg-icon" style="height: 64px; width: 64px;" viewBox="0 0 512 512" />
                              </div>
                              <div class="animated-column-title s_title a_position-relative a_font-weight-bold a_text-transform-up a_box-border" style="height: 46px;">
                                  <?php echo $pterm->name;?>
                              </div>
  			</div>
                          <p class="animated-column-desc s_desc a_top-100-per a_font-14 a_position-relative a_width-100-per a_line-25 a_box-border" style="height: 170px; top: 95.376%;"><?php echo $pterm->description; ?></p>
                          <div class="animated-column-btn a_position-relative a_top-100-per a_width-100-per" style="top: 100%;">
                              <div id="mk-button-8" class="mk-button-container _ relative    block text-center ">
                                  <span class="mk-button--text">Learn More</span>
                              </div>
                          </div>
                          </a>
                      </div>
  <style>
      .subcategory-bg<?php echo $loop; ?>:hover
          {
              background-color: <?php echo get_field('mouse_hover_cover','wpdmcategory_'.$term_id); ?> !important;
          }
  </style>
  		<?php  }
  ?>
  	</div>


  	</div>

   <?php } //mk_build_main_wrapper( mk_get_view('singular', 'wp-page', true) );


   else {

       //echo "No Child category....";

    //echo "<pre>"; print_r($cat);?>
    <div class="mk-main-wrapper-holder">
  	<div id="mk-page-id-6552" class="theme-page-wrapper mk-main-wrapper mk-grid full-layout  ">
  		<div class="theme-content" itemprop="mainEntityOfPage">
                      <div class="wpb_row vc_row vc_row-fluid  mk-fullwidth-false  attched-false     js-master-row  mk-in-viewport">
                          <?php
                      //$cat = $pterm->term_id;
                          if ( get_query_var('paged') ) {
                              $paged = get_query_var('paged');
                          } elseif ( get_query_var('page') ) { // 'page' is used instead of 'paged' on Static Front Page
                              $paged = get_query_var('page');
                          } else {
                              $paged = 1;
                          }
                          $args = array(
                              //'posts_per_page' => -1,
                              'post_type' => 'wpdmpro',
                               'pagination' => true,
                              'posts_per_page'=> '8',
                              'paged' => $paged,
                              'number'=> '1',
                              'tax_query' => array(
                                  array(
                                      'taxonomy' => 'wpdmcategory',
                                      'pagination' => true,

                                      'field' => 'term_id',
                                      'terms' =>  $_GET['cat'],
                                  )
                              )
                          );
                      query_posts($args);
                      if ( have_posts() ) {
                      while ( have_posts() ) : the_post();
                      setup_postdata( $args );?>
              <div style="" class="vc_col-sm-3 wpb_column column_container  _ height-full">
                      <div id="text-block-2" class="mk-text-block   ">
                              <div class="w3eden"><!-- WPDM Link Template: Call to Action 4 -->
                                  <blockquote class="well c2a4" style="background-color: #e5f6fd;">
                                      <div class="media text-center" style="background-color: #e5f6fd;">
                                          <div class="text-center">
                                              <img class="wpdm_icon" alt="Icon" src="http://noveosdx.com/wp-content/uploads/2017/11/document.png">
                                          </div>
                                          <div class="media-body additional-title">
                                              <h3 ><?php the_title(); ?></h3>
                                              <?php echo get_field('description') ?>
                                                          <?php
                                                          global $post;
                                                                          $packSize 	= get_post_meta($post->ID,'__wpdm_package_size',true);
                                                                      $downCount 	= get_post_meta($post->ID,'__wpdm_download_count',true);
                                                          ?>
                                          <div class="text-info" style="font-weight:300;font-size: 10pt;margin-bottom: 15px"><i style="margin: 2px 0 0 5px;opacity:0.5" class="fa fa-th-large"></i>
                                                          <?php if($packSize == ''){echo "0.00 KB";}else{echo $packSize;} ?>
                                                           <i style="margin: 2px 0 0 5px;opacity:0.5" class="fa fa-download"></i> <?php if($downCount == ''){echo "0";}else{echo $downCount;} ?> downloads</div>
                                          </div>
                              <div id="mk-gradient-button-7" class="mk-gradient-button fullwidth-false btn-align-center" >
                                  <?php
                                                          global $post1;
                                                                  $packSize 	= get_post_meta($post1->ID,'__wpdm_package_size',true);
                                                                      $downlink 	= get_post_meta($post1->ID,'__wpdm_download_link',true);
                                                          ?>
                                  <?php //foreach ( $parent_terms as $pterm ) ?>
                                  <a onclick="pluscount(<?php echo $post->ID; ?>);" href='<?php echo get_field('download').$pterm->term_id; ?>' class="mk-button mk-button--dimension-outline mk-button--size-large mk-button--corner-pointed light-skin" target="_blank" style="color: #01a8d5;" download>
                                      <span class="text">Download</span>
                                  </a>
                                  <?php //echo do_shortcode("[wpdm_package id='7083']"); ?>
                              </div>
                          </div>
                      </blockquote>

             </div>
      </div>
  </div>


                      <?php endwhile;

                         } else { ?>
                          <div style="" class="vc_col-sm-12 wpb_column column_container  _ height-full">
                      <div id="text-block-2" class="mk-text-block   ">
                              <div class="w3eden"><!-- WPDM Link Template: Call to Action 4 -->
                                  <blockquote class="well c2a4" style="background-color: #e5f6fd;">
                                      <div class="media text-center" style="background-color: #e5f6fd;">
                                          <div class="text-center">
                                             <?php echo wpautop( 'Sorry, no posts were found' );?>
                                          </div>
                          </div>
                      </blockquote>

             </div>
      </div>
  </div>
                         <?php } wp_reset_postdata();?>
  	</div>
      </div>
              <?php if ( have_posts() ) {?>
              <div class="pagination" style="display: flex;">
                  <div class="previous-page"><?php previous_posts_link( 'Previous' );?></div>
                  <div class="next-page"><?php next_posts_link( 'Next', $query->max_num_pages ); ?></div>
              </div>
              <?php }?>
  </div>
       </div>

   <?php } ?>
     <style>
        .well.c2a3 .btn.wpdm-download-link{ padding: 11px 30px;font-size: 11pt; }
        .well.c2a3 .media-body{ font-size: 11pt; }
        .well.c2a3 .wpdm_icon{ height: 42px; width: auto; }

        blockquote:before {
      position: absolute;
      background-image : url(data:image/svg+xml; utf8, <svg class="mk-svg-icon" xmlns="http://www.w3.org�-181 75t-75 181v32q0 40 28 68t68 28h224q80 0 136 56t56 136z"></path></svg>);
      content: "";
      display: none;
      left: 20px;
      top: 20px;
      width: 32px;
      height: 35px;

  }
    </style>


    <?php }

   add_shortcode( 'resource_category', 'get_resource_category' );
  /**********************/
  // Breadcrumbs
  function custom_breadcrumbs() {

      // Settings
      $separator          = '&gt;';
      $breadcrums_id      = 'breadcrumbs';
      $breadcrums_class   = 'breadcrumbs';
      $home_title         = 'Home';

      // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
      $custom_taxonomy    = 'product_cat';

      // Get the query & post information
      global $post,$wp_query;

      // Do not display on the homepage
      if ( !is_front_page() ) {

          // Build the breadcrums
          echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

          // Home page
          echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" >' . $home_title . '</a></li>';
          echo '<li class="separator separator-home"> ' . $separator . ' </li>';

          if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {

              echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';

          }

          if (is_page('resources-category-2') && (isset($_GET['cat'])) ) {
              //$term->name;
              echo '<li class="item-current item-archive"><strong class="bread-current bread-archive"><a href="'. get_page_link(6515).'">' .get_the_title(6515). '</a></strong></li>';

  			//echo '<li class="separator separator-home"> ' . $separator . ' </li>';


          $term = get_term_by('id', $_GET['cat'], 'wpdmcategory' );
          $tmpTerm = $term;
          $tmpCrumbs = array();
          while ($tmpTerm->parent > 0){
              $tmpTerm = get_term($tmpTerm->parent, get_query_var("taxonomy"));
              $crumb = '<li><a href="' . get_term_link($tmpTerm, get_query_var('taxonomy')) . '">' . $tmpTerm->name . '</a></li>';
              array_push($tmpCrumbs, $crumb);
              echo '<li class="separator separator-home"> ' . $separator . ' </li>';
          }
          echo implode('<li class="separator separator-home">'.$separator.'</li>',array_reverse($tmpCrumbs));

  		echo '<li class="separator separator-home"> ' . $separator . ' </li>';

          //echo '<li class="item-current item-archive"><strong class="bread-current bread-archive"><a href="' . get_term_link($tmpTerm, get_query_var('taxonomy')) . '">' . $term->name . '</a></li>';
          echo '<li class="item-current item-archive 123"><strong class="bread-current bread-archive"><a>' . $term->name . '</a></li>';

                        //echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' .$term->name. '</strong></li>';
                        //echo '<li class="separator separator-home"> ' . $separator . ' </li>';

      }
          else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {

              // If post is a custom post type
              $post_type = get_post_type();

              // If it is a custom post type display name and link
              if($post_type != 'post') {

                  $post_type_object = get_post_type_object($post_type);
                  $post_type_archive = get_post_type_archive_link($post_type);

                  echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '">' . $post_type_object->labels->name . '</a></li>';
                  echo '<li class="separator"> ' . $separator . ' </li>';

              }

              $custom_tax_name = get_queried_object()->name;
              echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';

          } else if ( is_single() ) {

              // If post is a custom post type
              $post_type = get_post_type();

              // If it is a custom post type display name and link
              if($post_type != 'post') {

                  $post_type_object = get_post_type_object($post_type);
                  $post_type_archive = get_post_type_archive_link($post_type);

                  echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" >' . $post_type_object->labels->name . '</a></li>';
                  echo '<li class="separator"> ' . $separator . ' </li>';

              }

              // Get post category info
              $category = get_the_category();

              if(!empty($category)) {

                  // Get last category post is in
                  $last_category = end(array_values($category));

                  // Get parent any categories and create array
                  $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                  $cat_parents = explode(',',$get_cat_parents);

                  // Loop through parent categories and store in variable $cat_display
                  $cat_display = '';
                  foreach($cat_parents as $parents) {
                      $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                      $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                  }

              }

              // If it's a custom post type within a custom taxonomy
              $taxonomy_exists = taxonomy_exists($custom_taxonomy);
              if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

                  $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                  $cat_id         = $taxonomy_terms[0]->term_id;
                  $cat_nicename   = $taxonomy_terms[0]->slug;
                  $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                  $cat_name       = $taxonomy_terms[0]->name;

              }

              // Check if the post is in a category
              if(!empty($last_category)) {
                  echo $cat_display;
                  echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" >' . get_the_title() . '</strong></li>';

              // Else if post is in a custom taxonomy
              } else if(!empty($cat_id)) {

                  echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" >' . $cat_name . '</a></li>';
                  echo '<li class="separator"> ' . $separator . ' </li>';
                  echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" >' . get_the_title() . '</strong></li>';

              } else {

                  echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" >' . get_the_title() . '</strong></li>';

              }

          } else if ( is_category() ) {

              // Category page
              echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

          } else if ( is_page() ) {

              // Standard page
              if( $post->post_parent ){

                  // If child page, get parents
                  $anc = get_post_ancestors( $post->ID );

                  // Get parents in the right order
                  $anc = array_reverse($anc);

                  // Parent page loop
                  if ( !isset( $parents ) ) $parents = null;
                  foreach ( $anc as $ancestor ) {
                    if($ancestor == '7875') {continue;}
                      $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" >' . get_the_title($ancestor) . '</a></li>';
                      $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                  }

                  // Display parent pages
                  echo $parents;

                  // Current page
                  echo '<li class="item-current item-' . $post->ID . '"><strong> ' . get_the_title() . '</strong></li>';

              } else {

                  // Just display current page if not parents
                  echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';

              }

          } else if ( is_tag() ) {

              // Tag page

              // Get tag information
              $term_id        = get_query_var('tag_id');
              $taxonomy       = 'post_tag';
              $args           = 'include=' . $term_id;
              $terms          = get_terms( $taxonomy, $args );
              $get_term_id    = $terms[0]->term_id;
              $get_term_slug  = $terms[0]->slug;
              $get_term_name  = $terms[0]->name;

              // Display the tag name
              echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';

          } elseif ( is_day() ) {

              // Day archive

              // Year link
              echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" >' . get_the_time('Y') . ' Archives</a></li>';
              echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

              // Month link
              echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" >' . get_the_time('M') . ' Archives</a></li>';
              echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';

              // Day display
              echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';

          } else if ( is_month() ) {

              // Month Archive

              // Year link
              echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" >' . get_the_time('Y') . ' Archives</a></li>';
              echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

              // Month display
              echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" >' . get_the_time('M') . ' Archives</strong></li>';

          } else if ( is_year() ) {

              // Display year archive
              echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" >' . get_the_time('Y') . ' Archives</strong></li>';

          } else if ( is_author() ) {

              // Auhor archive

              // Get the author information
              global $author;
              $userdata = get_userdata( $author );

              // Display author name
              echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" >' . 'Author: ' . $userdata->display_name . '</strong></li>';

          } else if ( get_query_var('paged') ) {

              // Paginated archives
              echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" >'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';

          } else if ( is_search() ) {

              // Search results page
              echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" >Search results for: ' . get_search_query() . '</strong></li>';

          } elseif ( is_404() ) {

              // 404 page
              echo '<li>' . 'Error 404' . '</li>';
          }

          echo '</ul>';

      }

  }
  add_shortcode( 'breadcrumbs', 'custom_breadcrumbs' );
  /*****************************/
  // custom_heading_title
  function custom_heading_title() {
      $custom_taxonomy    = 'product_cat';

      global $post,$wp_query,$separator,$crumb;
      if ( !is_front_page() ) {
  		//echo $separator ;
  			if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
  				echo '' . post_type_archive_title($prefix, false) . '';
  			}

          if (is_page('resources-category-2') && (isset($_GET['cat'])) ) {
              //echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' .get_the_title(6515). '</strong></li>';
              echo ' ' . $separator . ' ';
          $term = get_term_by('id', $_GET['cat'], 'wpdmcategory' );
          $tmpTerm = $term;
          $tmpCrumbs = array();
          while ($tmpTerm->parent > 0){
              $tmpTerm = get_term($tmpTerm->parent, get_query_var("taxonomy"));
              //$crumb = '<li><a href="' . get_term_link($tmpTerm, get_query_var('taxonomy')) . '">' . $tmpTerm->name . '</a></li>';
              array_push($tmpCrumbs, $crumb);
              //echo '<li class="separator separator-home"> ' . $separator . ' </li>';
          }
          echo implode('' . $separator . ' ', array_reverse($tmpCrumbs));

          echo '' . $term->name . '';

                        //echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' .$term->name. '</strong></li>';
                        //echo '<li class="separator separator-home"> ' . $separator . ' </li>';

      }
          else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              // If post is a custom post type
              $post_type = get_post_type();
              // If it is a custom post type display name and link
              if($post_type != 'post') {
                  $post_type_object = get_post_type_object($post_type);
                  $post_type_archive = get_post_type_archive_link($post_type);
                  //echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" >' . $post_type_object->labels->name . '</a></li>';
                  echo '' . $separator . '';
              }
              $custom_tax_name = get_queried_object()->name;
              echo '' . $custom_tax_name . '';
          } else if ( is_single() ) {
              // If post is a custom post type
              $post_type = get_post_type();
              // If it is a custom post type display name and link
              if($post_type != 'post') {
                  $post_type_object = get_post_type_object($post_type);
                  $post_type_archive = get_post_type_archive_link($post_type);
                  echo '' . $post_type_object->labels->name . '';
                  echo '' . $separator . '';
              }
              // Get post category info
              $category = get_the_category();
              if(!empty($category)) {
                  // Get last category post is in
                  $last_category = end(array_values($category));
                  // Get parent any categories and create array
                  $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                  $cat_parents = explode(',',$get_cat_parents);
                  // Loop through parent categories and store in variable $cat_display
                  $cat_display = '';
                  foreach($cat_parents as $parents) {
                      $cat_display .= ''.$parents.'';
                      //$cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                  }
              }
              // If it's a custom post type within a custom taxonomy
              $taxonomy_exists = taxonomy_exists($custom_taxonomy);
              if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                  $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                  $cat_id         = $taxonomy_terms[0]->term_id;
                  $cat_nicename   = $taxonomy_terms[0]->slug;
                  $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                  $cat_name       = $taxonomy_terms[0]->name;
              }
              // Check if the post is in a category
              if(!empty($last_category)) {
                  echo $cat_display;
                  echo '' . get_the_title() . '';
              // Else if post is in a custom taxonomy
              } else if(!empty($cat_id)) {
                  echo '' . $cat_name . '';
                  echo ' ' . $separator . ' ';
                  echo '' . get_the_title() . '';
              } else {
                  echo '' . get_the_title() . '';
              }
          } else if ( is_category() ) {
              // Category page
              echo '' . single_cat_title('', false) . '';
          } else if ( is_page() ) {
              // Standard page
              if( $post->post_parent ){
                  // If child page, get parents
                  $anc = get_post_ancestors( $post->ID );
                  // Get parents in the right order
                  $anc = array_reverse($anc);
                  // Parent page loop
                  if ( !isset( $parents ) ) $parents = null;
                  foreach ( $anc as $ancestor ) {
                      $parents .= '' . get_the_title($ancestor) . '';
                      $parents .= '' . $separator . '';
                  }
                  // Display parent pages
                  echo $parents;
                  // Current page
  				echo '' . get_the_title() . '';
              } else {
                  // Just display current page if not parents
                  echo '' . get_the_title() . '';
              }
          } else if ( is_tag() ) {
              // Tag page
              // Get tag information
              $term_id        = get_query_var('tag_id');
              $taxonomy       = 'post_tag';
              $args           = 'include=' . $term_id;
              $terms          = get_terms( $taxonomy, $args );
              $get_term_id    = $terms[0]->term_id;
              $get_term_slug  = $terms[0]->slug;
              $get_term_name  = $terms[0]->name;
              // Display the tag name
              echo '' . $get_term_name . '';
          } elseif ( is_day() ) {
              // Day archive
              // Year link
              echo '' . get_the_time('Y') . ' Archives';
              echo '' . $separator . '';
              // Month link
              echo '' . get_the_time('M') . ' Archives';
              echo '' . $separator . '';
              // Day display
              echo ' ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives';
          } else if ( is_month() ) {
              // Month Archive
              // Year link
              echo '' . get_the_time('Y') . ' Archives';
              echo '' . $separator . '';
              // Month display
              echo '' . get_the_time('M') . ' Archives';
  			} else if ( is_year() ) {
              // Display year archive
              echo '' . get_the_time('Y') . ' Archives';
          } else if ( is_author() ) {
              // Auhor archive
              // Get the author information
              global $author;
              $userdata = get_userdata( $author );
              // Display author name
              echo '' . 'Author: ' . $userdata->display_name . '';
          } else if ( get_query_var('paged') ) {
              // Paginated archives
              echo ''.__('Page') . ' ' . get_query_var('paged') . '';
          } else if ( is_search() ) {
              // Search results page
              echo '' . get_search_query() . '';
          } elseif ( is_404() ) {
              echo '' . 'Error 404' . '';
          }
      }

  }
  add_shortcode( 'resource_custom_heading', 'custom_heading_title' );
  function mark_menu_item_as_active($classes, $item) {
      if( in_array('my-custom-class',$classes) && ( isset($_GET['cat']) ) )   {
          $classes[] = 'current-menu-parent';
      }
      return $classes;
  }
  add_filter('nav_menu_css_class', 'mark_menu_item_as_active', 10, 2);

  function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');

  // functions.php

  add_action( 'init', 'update_my_custom_type', 99 );
   /**
   * update_my_custom_type
   *
   * @author  Joe Sexton <joe@webtipblog.com>
   */
  function update_my_custom_type() {
      global $wp_post_types;

      if ( post_type_exists( 'wpdmpro' ) ) {

          // exclude from search results
          $wp_post_types['wpdmpro']->exclude_from_search = true;
      }
  }

  //disable gutenberg editor
  add_filter('use_block_editor_for_post', '__return_false');

  add_filter( 'user_registration_login_redirect', 'ur_redirect_after_login', 10, 2 );

  function ur_redirect_after_login( $redirect, $user ) {
    if(isset($_GET['redirect_to'])) {
      return $_GET['redirect_to'];
    }
    return home_url();
  }

  function noveosdx_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
          background-image: url('/wp-content/uploads/2017/11/logo.png');
      		height:48px;
      		width:135px;
      		background-size: cover;
      		background-repeat: no-repeat;
      	  padding-bottom: 0;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'noveosdx_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'NoveosDX';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style-login.css' );
    // wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/style-login.js' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

function custom_login_title( $login_title ) {
return str_replace(array(' &#8212; WordPress'), array(''),$login_title );
}
add_filter( 'login_title', 'custom_login_title' );

?>
