<?php

global $mk_options;



$mk_footer_class = $show_footer = $disable_mobile = $footer_status = '';



$post_id = global_get_post_id();

if($post_id) {

  $show_footer = get_post_meta($post_id, '_template', true );

  $cases = array('no-footer', 'no-header-footer', 'no-header-title-footer', 'no-footer-title');

  $footer_status = in_array($show_footer, $cases);

}



if($mk_options['disable_footer'] == 'false' || ( $footer_status )) {

  $mk_footer_class .= ' mk-footer-disable';

}



if($mk_options['footer_type'] == '2') {

  $mk_footer_class .= ' mk-footer-unfold';

}





$boxed_footer = (isset($mk_options['boxed_footer']) && !empty($mk_options['boxed_footer'])) ? $mk_options['boxed_footer'] : 'true';

$footer_grid_status = ($boxed_footer == 'true') ? ' mk-grid' : ' fullwidth-footer';

$disable_mobile = ($mk_options['footer_disable_mobile'] == 'true' ) ? $mk_footer_class .= ' disable-on-mobile'  :  ' ';



?>



<section id="mk-footer-unfold-spacer"></section>



<section id="mk-footer" class="<?php echo $mk_footer_class; ?> footer-main" <?php echo get_schema_markup('footer'); ?>>

    <?php if($mk_options['disable_footer'] == 'true' && !$footer_status) : ?>

    <div class="footer-wrapper<?php echo $footer_grid_status;?>">

        <div class="mk-padding-wrapper">

            <?php mk_get_view('footer', 'widgets'); ?>

            <div class="clearboth"></div>

        </div>

    </div>

    <?php endif;?>

    <?php if ( $mk_options['disable_sub_footer'] == 'true' && ! $footer_status ) {

        mk_get_view( 'footer', 'sub-footer', false, ['footer_grid_status' => $footer_grid_status] );

    } ?>

</section>

</div>

<?php

    global $is_header_shortcode_added;



    if ( $mk_options['seondary_header_for_all'] === 'true' || get_header_style() === '3' || $is_header_shortcode_added === '3' ) {

        mk_get_header_view('holders', 'secondary-menu', ['header_shortcode_style' => $is_header_shortcode_added]);

    }

?>

</div>



<div class="bottom-corner-btns js-bottom-corner-btns">

<?php

    if ( $mk_options['go_to_top'] != 'false' ) {

        mk_get_view( 'footer', 'navigate-top' );

    }



    if ( $mk_options['disable_quick_contact'] != 'false' ) {

        mk_get_view( 'footer', 'quick-contact' );

    }



    do_action('add_to_cart_responsive');

?>

</div>





<?php if ( $mk_options['header_search_location'] === 'fullscreen_search' ) {

    mk_get_header_view('global', 'full-screen-search');

} ?>



<?php if (!empty($mk_options['body_border']) && $mk_options['body_border'] === 'true') { ?>

    <div class="border-body border-body--top"></div>

    <div class="border-body border-body--left border-body--side"></div>

    <div class="border-body border-body--right border-body--side"></div>

    <div class="border-body border-body--bottom"></div>

<?php } ?>



    <?php

    wp_footer();



    ?>



    <script>

        !function(e){var a=window.location,n=a.hash;if(n.length&&n.substring(1).length){var r=e(".vc_row, .mk-main-wrapper-holder, .mk-page-section, #comments"),t=r.filter("#"+n.substring(1));if(!t.length)return;n=n.replace("!loading","");var i=n+"!loading";a.hash=i}}(jQuery);

    </script>



    <?php

    // Asks W3C Total Cache plugin to move all JS and CSS assets to before body closing tag.

    //if(defined('W3TC')) {

    //    echo "<!-- W3TC-include-js-head -->";

    //    echo "<!-- W3TC-include-css -->";

//    }

    ?>



    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/themes/jupiter/js/grid.js'></script>

      <script>

        jQuery(document).ready( function(){

            if(jQuery('#og-grid').length){

			jQuery(function() {

				Grid.init();

			});

			setTimeout(function(){

				jQuery('.og-grid li a').matchHeight();

			},300)

		};

        });

    </script>

    <?php if( (!isset($_SESSION['popup'])) ){ ?>
<style>
html, body {
      overflow: hidden;
 }
</style>

<script type="text/javascript">
	jQuery(".close").click(function(){
      var lang = jQuery(this).data('lang');
	    jQuery(".overlay").removeClass("open");

             var url = '';
             if(
               (lang == 'us')
               &&
               !(jQuery('body').hasClass('category'))
               &&
               !(jQuery('body').hasClass('single-post'))
             ) {
               url = '/us' + window.location.pathname;
             } else {
               url = window.location.href;
             }

            if (url.indexOf('?') > -1){

               url += '&popup='+lang;

            }else{

               url += '?popup='+lang;

            }

            window.location.href = url;

            //alert("close call");

	});





	function openCity(evt, cityName) {

    // Declare all variables

    var i, tabcontent, tablinks;



    // Get all elements with class="tabcontent" and hide them

    tabcontent = document.getElementsByClassName("tabcontent");

    for (i = 0; i < tabcontent.length; i++) {

        tabcontent[i].style.display = "none";

    }



    // Get all elements with class="tablinks" and remove the class "active"

    tablinks = document.getElementsByClassName("tablinks");

    for (i = 0; i < tablinks.length; i++) {

        tablinks[i].className = tablinks[i].className.replace(" active", "");

    }



    // Show the current tab, and add an "active" class to the button that opened the tab

    document.getElementById(cityName).style.display = "block";

    evt.currentTarget.className += " active";

}

</script>

<?php }?>

<?php if(isset($_GET['cat']) && ($_GET['cat']==52)) { ?>

    <script>

        jQuery(document).ready( function(){

            jQuery('#menu-item-7046').addClass('current-menu-item');

        });

    </script>

<?php } ?>



<?php if(isset($_GET['cat']) && ($_GET['cat']==53)) { ?>

    <script>

        jQuery(document).ready( function(){

            jQuery('#menu-item-7047').addClass('current-menu-item');

        });

    </script>

<?php } ?>



<?php if(isset($_GET['cat']) && ($_GET['cat']==56)) { ?>

    <script>

        jQuery(document).ready( function(){

            jQuery('#menu-item-7050').addClass('current-menu-item');

        });

    </script>

<?php } ?>

<style>

    .main_menu li.current-menu-item a, .main_menu li.current-menu-ancestor a{

    color: #00c8d7 !important; /* highlight color */

}

    </style>

    <?php if (is_page('resources-2')) {   //  displaying a child page ?>

    <script type="text/javascript">

        jQuery("li.current-page-ancestor").addClass('current-menu-item');

    </script>

<?php } ?>

<script>
        function getParameterByName(name, url) {
          if (!url) url = window.location.href;
          name = name.replace(/[\[\]]/g, '\\$&');
          var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
          results = regex.exec(url);
          if (!results) return null;
          if (!results[2]) return '';
          return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }
        jQuery(document).ready( function(){
          jQuery("a").on("click touchend", function(e) {
             var el = jQuery(this);
             var link = el.attr("href");
             window.location = link;
          });

          if(jQuery('.resource-login').length > 0) {
              var redirect_to = getParameterByName('redirect_to');
              var href = jQuery('.resource-login').attr('href');
              if(redirect_to != null) {
                if(href.indexOf("?") > 0) {
                  href = href + "&redirect_to=" + encodeURIComponent(redirect_to);
                } else {
                  href = href + "?redirect_to=" + encodeURIComponent(redirect_to);
                }

                if(href != 'null') {
                    jQuery('.resource-login').attr('href', href);
                }
              }
          }
        });
</script>
<script type="text/javascript">
  var lang = jQuery('body').data('lang');
  if(lang == 'us') {
    jQuery('.header-logo a').attr({'href': '/us/'});
  }
</script>
<script type="text/javascript">
  jQuery('body.single-post #menu-item-7990, body.single-post #menu-item-7988').addClass('current-menu-item current_page_item');
</script>
</body>

</html>
