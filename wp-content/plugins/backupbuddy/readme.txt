=== BackupBuddy ===
Requires at least: 3.9
Tested up to: 4.9.6
Stable tag: 8.2.7.0
Contributors: ithemes, dustinbolton, blepoxp, Jeremy Trask, briandichiara
Website: http://ithemes.com/purchase/backupbuddy/

== Description ==

The most complete WordPress solution for Backup, Restoration, Migration, and Deployment to the same host or a new domain. Backs up a customizable selection of files, settings, and content for a complete snapshot of your site. Stash Live feature allows for real-time live backups to the cloud.

== Installation: ==
1. Download and unzip the latest release zip file
1. If you use the WordPress plugin uploader to install this plugin skip to step 4.
1. Upload the entire plugin directory to your `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress Administration

= Usage =
1. Navigate to the new menu for this plugin in the WordPress Administration Panel
2. Select 'Getting Started' for instructions and additional information.
