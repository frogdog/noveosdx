<?php
   /*
   Plugin Name: NoveosDX
   Plugin URI: https://noveosdx.com
   description: Custom Functionality
   Version: 1.0
   Author: Will Tate
   Author URI: https://allea.rs
   License: GPL2
   */

add_action('manage_wpdmpro_posts_columns', 'noveosdx_wpdmpro_columns');
function noveosdx_wpdmpro_columns($columns) {
   $columns['lang'] = __( 'Language' );
   unset($columns['image']);
   unset($columns['comments']);
   unset($columns['wpdmshortcode']);
   unset($columns['download_count']);
   return $columns;
}

add_action('manage_wpdmpro_posts_custom_column', 'noveosdx_wpdmpro_custom_column', 10, 2);
function noveosdx_wpdmpro_custom_column($column, $post_id) {
  if('lang' == $column) {
      echo '<div id="lang-' . $post_id . '">' . get_post_meta($post_id, 'language', true) . '</div>';
  }
}

add_action('manage_edit-wpdmpro_sortable_columns', 'noveosdx_wpdmpro_sortable_columns');
function noveosdx_wpdmpro_sortable_columns($columns) {
   $columns['taxonomy-wpdmcategory'] = 'taxonomy-wpdmcategory';
   return $columns;
}

add_action('bulk_edit_custom_box', 'noveosdx_add_quick_edit', 10, 2);
add_action('quick_edit_custom_box', 'noveosdx_add_quick_edit', 10, 2);
function noveosdx_add_quick_edit($column, $post_type) {
   if($post_type = 'wpdmpro') {
      switch($column) {
         case 'lang':
         ?>
         <fieldset class="inline-edit-col-right">
            <div class="inline-edit-group">
               <label>
                  <span class="title">Language</span>
                  <select name="lang">
                     <option value=''></option>
                     <?php
                        $language = get_field_object('field_5bd7281b3c03f');
                        $options = $language['choices'];
                        foreach($options as $key => $value) {
                           echo "<option value='{$key}'>{$value}</option>";
                        }
                     ?>
                  </select>
               </label>
            </div>
         </fieldset>
         <?php
      }
   }
}

add_action('save_post', 'noveosdx_quick_edit_save');
function noveosdx_quick_edit_save($post_id) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
 
    if ( ! current_user_can( 'edit_post', $post_id ) || 'wpdmpro' != $_POST['post_type'] ) {
        return $post_id;
    }

    update_post_meta($post_id, 'language', $_POST['lang']);
}

add_action( 'wp_ajax_noveosdx_save_bulk_edit', 'noveosdx_save_bulk_edit' );
function noveosdx_save_bulk_edit() {
   // get our variables
   $post_ids = ( isset( $_POST[ 'post_ids' ] ) && !empty( $_POST[ 'post_ids' ] ) ) ? $_POST[ 'post_ids' ] : array();
   $lang = ( isset( $_POST[ 'lang' ] ) && !empty( $_POST[ 'lang' ] ) ) ? $_POST[ 'lang' ] : NULL;
   // if everything is in order
   if ( !empty( $post_ids ) && is_array( $post_ids ) && !empty( $lang ) ) {
      foreach( $post_ids as $post_id ) {
         update_post_meta( $post_id, 'language', $lang );
      }
   }
}

add_action( 'restrict_manage_posts', 'noveosdx_filter_wpdmpro_by_taxonomies' , 10, 2);
function noveosdx_filter_wpdmpro_by_taxonomies( $post_type, $which ) {

   // Apply this only on a specific post type
   if ( 'wpdmpro' !== $post_type )
      return;

   // A list of taxonomy slugs to filter by
   $taxonomies = array( 'wpdmcategory');

   foreach ( $taxonomies as $taxonomy_slug ) {

      // Retrieve taxonomy data
      $taxonomy_obj = get_taxonomy( $taxonomy_slug );
      $taxonomy_name = $taxonomy_obj->labels->name;

      // Retrieve taxonomy terms
      $terms = get_terms( $taxonomy_slug );

      // Display filter HTML
      echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
      echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
      foreach ( $terms as $term ) {
         printf(
            '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
            $term->slug,
            ( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
            $term->name,
            $term->count
         );
      }
      echo '</select>';
   }

}

add_action('admin_print_scripts-edit.php', 'noveosdx_enqueue_edit_scripts');
function noveosdx_enqueue_edit_scripts() {
   wp_enqueue_script('noveosdx-admin-edit', plugins_url('noveosdx') .'/js/noveosdx.js', array( 'jquery', 'inline-edit-post' ), '', true);
}

?>